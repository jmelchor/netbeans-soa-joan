package net.openesb.netbeans.module.server.support.standalone.nodes;

import java.awt.Component;
import java.util.LinkedList;
import javax.swing.Action;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.customizer.Customizer;
import net.openesb.netbeans.module.server.support.standalone.nodes.actions.ViewWebConsoleAction;
import net.openesb.netbeans.module.server.support.standalone.nodes.actions.ViewServerLogAction;
import net.openesb.netbeans.module.server.support.standalone.util.StandaloneProperties;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneInstanceNode extends AbstractNode implements Node.Cookie {
    
    private final Lookup lookup;
    
    private static final String HTTP_HEADER = "http://";
    
    /**
     * Creates a new instance of StandaloneInstanceNode.
     * 
     * @param lookup will contain DeploymentFactory, DeploymentManager, Management objects. 
     */
    public StandaloneInstanceNode(Children children, Lookup lookup) {
        super(children);
        this.lookup = lookup;
        setIconBaseWithExtension("net/openesb/netbeans/module/server/support/standalone/nodes/openesb.png"); // NOI18N
        getCookieSet().add(this);
    }
    
    @Override
    public Action[] getActions(boolean context) {
        Action[]  newActions = new Action[3] ;
        newActions[0]= null;
        newActions[1]= (SystemAction.get(ViewWebConsoleAction.class));
	newActions[2]= (SystemAction.get(ViewServerLogAction.class));
        return newActions;
    }
    
    @Override
    public boolean hasCustomizer() {
        return true;
    }
    
    @Override
    public Component getCustomizer() {
        return new Customizer(getDeploymentManager());
    }
    
    @Override
    public Sheet createSheet(){
        Sheet sheet = super.createSheet();
        Sheet.Set properties = sheet.get(Sheet.PROPERTIES);
        if (properties == null) {
            properties = Sheet.createPropertiesSet();
            sheet.put(properties);
        }
        final InstanceProperties ip = getDeploymentManager().getInstanceProperties();
        
        Node.Property property=null;
        
        // DISPLAY NAME
        property = new PropertySupport.ReadOnly(
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_DISPLAY_NAME"), //NOI18N
                String.class,
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_DISPLAY_NAME"),   // NOI18N
                NbBundle.getMessage(StandaloneInstanceNode.class, "HINT_DISPLAY_NAME")   // NOI18N
                ) {
            public Object getValue() {
                return ip.getProperty(StandaloneProperties.PROP_DISPLAY_NAME);
            }
        };
        
        properties.put(property);
        
        // servewr name
        property = new PropertySupport.ReadOnly(
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_INSTANCE_NAME"),    //NOI18N
                String.class,
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_INSTANCE_NAME"),   // NOI18N
                NbBundle.getMessage(StandaloneInstanceNode.class, "HINT_INSTANCE_NAME")   // NOI18N
                ) {
            public Object getValue() {
                return ip.getProperty(StandaloneProperties.PROPERTY_INSTANCE_NAME);
            }
        };
        properties.put(property);
        
        //server location
        property = new PropertySupport.ReadOnly(
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_SERVER_PATH"),   //NOI18N
                String.class,
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_SERVER_PATH"),   // NOI18N
                NbBundle.getMessage(StandaloneInstanceNode.class, "HINT_SERVER_PATH")   // NOI18N
                ) {
            public Object getValue() {
                return ip.getProperty(StandaloneProperties.PROPERTY_ROOT_DIR);
            }
        };
        properties.put(property);
        
        //host
        property = new PropertySupport.ReadOnly(
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_HOST"),    //NOI18N
                String.class,
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_HOST"),   // NOI18N
                NbBundle.getMessage(StandaloneInstanceNode.class, "HINT_HOST")   // NOI18N
                ) {
            public Object getValue() {
                return ip.getProperty(StandaloneProperties.PROPERTY_HOST_NAME);
            }
        };
        properties.put(property);
        
        //port
        property = new PropertySupport.ReadOnly(
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_PORT"),    //NOI18N
                Integer.TYPE,
                NbBundle.getMessage(StandaloneInstanceNode.class, "LBL_PORT"),   // NOI18N
                NbBundle.getMessage(StandaloneInstanceNode.class, "HINT_PORT")   // NOI18N
                ) {
            public Object getValue() {
                return new Integer(ip.getProperty(StandaloneProperties.PROPERTY_ADMIN_PORT));
            }
        };
        properties.put(property);
        
        return sheet;
    }
    
    @Override
    public String getShortDescription() {
        InstanceProperties ip = InstanceProperties.getInstanceProperties(getDeploymentManager().getUri());
        String host = ip.getProperty(StandaloneProperties.PROPERTY_HOST_NAME);
        String port = ip.getProperty(StandaloneProperties.PROPERTY_ADMIN_PORT);
        return  HTTP_HEADER + host + ":" + port + "/"; // NOI18N
    }
    
    /** Return the StandaloneDeploymentManager instance this node represents. */
    public StandaloneDeploymentManager getDeploymentManager() {
        return ((StandaloneDeploymentManager) lookup.lookup(StandaloneDeploymentManager.class));
    }
}
