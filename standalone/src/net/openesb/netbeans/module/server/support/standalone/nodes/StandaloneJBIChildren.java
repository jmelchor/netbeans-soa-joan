package net.openesb.netbeans.module.server.support.standalone.nodes;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.nodes.actions.Refreshable;
import net.openesb.netbeans.module.server.support.standalone.util.Util;
import org.netbeans.modules.sun.manager.jbi.nodes.api.NodeExtensionProvider;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneJBIChildren extends StandaloneAsyncChildren implements Refreshable {

    private static final Logger LOGGER = Logger.getLogger(StandaloneJBIChildren.class.getName());

    private final Lookup lookup;

    StandaloneJBIChildren(Lookup lookup) {
	this.lookup = lookup;
    }

    @Override
    public void updateKeys() {
	setKeys(new Object[]{Util.WAIT_NODE});
	getExecutorService().submit(new StandaloneJBINodeUpdater(), 0);
    }

    class StandaloneJBINodeUpdater implements Runnable {

	List keys = new ArrayList();

	@Override
	public void run() {
	    StandaloneDeploymentManager sdm = lookup.lookup(StandaloneDeploymentManager.class);
	    if (sdm.getMBeanServerConnection() != null) {
		for (NodeExtensionProvider nep : Lookup.getDefault().lookupAll(NodeExtensionProvider.class)) {
		    if (nep != null) {
			Node node = nep.getExtensionNode(sdm.getMBeanServerConnection());
			if (node != null) {
			    keys.add(node);
			}
		    }
		}
	    }

	    setKeys(keys);
	}
    }

    @Override
    protected void addNotify() {
	updateKeys();
    }

    @Override
    protected void removeNotify() {
	setKeys(java.util.Collections.EMPTY_SET);
    }

    @Override
    protected org.openide.nodes.Node[] createNodes(Object key) {
	if (key instanceof Node) {
	    return new Node[]{(Node) key};
	}

	if (key instanceof String && key.equals(Util.WAIT_NODE)) {
	    return new Node[]{Util.createWaitNode()};
	}

	return null;
    }
}
