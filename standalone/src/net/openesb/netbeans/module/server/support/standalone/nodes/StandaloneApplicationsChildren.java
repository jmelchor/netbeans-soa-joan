package net.openesb.netbeans.module.server.support.standalone.nodes;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneApplicationsChildren extends Children.Keys {
    
    StandaloneApplicationsChildren(Lookup lookup) {
	// We have only a JBI Node at this time
	// Later, we can add datasource here
        setKeys(new Object[] {createJBINode(lookup)});
    }
    
    @Override
    protected void addNotify() {
    }
    
    @Override
    protected void removeNotify() {
    }
    
    @Override
    protected org.openide.nodes.Node[] createNodes(Object key) {
        if (key instanceof AbstractNode){
            return new Node[]{(AbstractNode)key};
        }
        
        return null;
    }
    
    /*
     * Creates an EAR Applications parent node
     */
    public static StandaloneItemNode createJBINode(Lookup lookup) {
        return new StandaloneItemNode(new StandaloneJBIChildren(lookup), NbBundle.getMessage(StandaloneTargetNode.class, "LBL_JBI"));
    }
}