package net.openesb.netbeans.module.server.support.standalone.nodes;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneTargetNode extends AbstractNode {
    
    public StandaloneTargetNode(final Lookup lookup) {
	super(new Children.Array());
	getChildren().add(new Node[] { new StandaloneItemNode(new StandaloneJBIChildren(lookup), NbBundle.getMessage(StandaloneTargetNode.class, "LBL_Apps"))});
    }
}
