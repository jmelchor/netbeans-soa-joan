package net.openesb.netbeans.module.server.support.standalone.config;

import java.io.InputStream;
import java.io.OutputStream;
import javax.enterprise.deploy.model.DDBeanRoot;
import javax.enterprise.deploy.model.DeployableObject;
import javax.enterprise.deploy.spi.DConfigBeanRoot;
import javax.enterprise.deploy.spi.DeploymentConfiguration;
import javax.enterprise.deploy.spi.exceptions.BeanNotFoundException;
import javax.enterprise.deploy.spi.exceptions.ConfigurationException;

/**
 *
 * @author David BRASSELY
 */
public class StandaloneConfiguration implements DeploymentConfiguration {

    private DeployableObject deplObj;
    
    public StandaloneConfiguration (DeployableObject deplObj) {
        this.deplObj = deplObj;
    }
    
    // JSR-88 methods ---------------------------------------------------------
    
    @Override
    public DeployableObject getDeployableObject() {
        return deplObj;
    }

    @Override
    public DConfigBeanRoot getDConfigBeanRoot(DDBeanRoot ddbr) throws ConfigurationException {
        return null;
    }

    @Override
    public void removeDConfigBean(DConfigBeanRoot dcbr) throws BeanNotFoundException {
    }

    @Override
    public DConfigBeanRoot restoreDConfigBean(InputStream in, DDBeanRoot ddbr) throws ConfigurationException {
        return null;
    }

    @Override
    public void saveDConfigBean(OutputStream out, DConfigBeanRoot dcbr) throws ConfigurationException {
    }

    @Override
    public void restore(InputStream in) throws ConfigurationException {
    }

    @Override
    public void save(OutputStream out) throws ConfigurationException {
    }
    
}
