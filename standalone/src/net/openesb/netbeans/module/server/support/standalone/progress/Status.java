package net.openesb.netbeans.module.server.support.standalone.progress;

import javax.enterprise.deploy.shared.ActionType;
import javax.enterprise.deploy.shared.CommandType;
import javax.enterprise.deploy.shared.StateType;
import javax.enterprise.deploy.spi.status.DeploymentStatus;

/**
 * Basic implementation of DeploymentStatus.
 * 
 * @author David BRASSELY
 */
public class Status implements DeploymentStatus {

    /** Value of action type. */
    private ActionType at;

    /** Executed command. */
    private CommandType ct;

    /** Status message. */
    private String msg;
    
    /** Current state. */
    private StateType state;
    
    public Status (ActionType at, CommandType ct, String msg, StateType state) {
        this.at = at;
        this.ct = ct;
        int lastEx;
        if ((lastEx = msg.lastIndexOf ("Exception:")) > 0) {  //NOI18N
            this.msg = msg.substring (lastEx + "Exception:".length());  //NOI18N
        } else {
            this.msg = msg;
        }
        this.state = state;
    }
    
    public ActionType getAction () {
        return at;
    }
    
    public CommandType getCommand () {
        return ct;
    }
    
    public String getMessage () {
        return msg;
    }
    
    public StateType getState () {
        return state;
    }
    
    public boolean isCompleted () {
        return StateType.COMPLETED.equals (state);
    }
    
    public boolean isFailed () {
        return StateType.FAILED.equals (state);
    }
    
    public boolean isRunning () {
        return StateType.RUNNING.equals (state);
    }
    
    public String toString () {
        return "A="+getAction ()+" S="+getState ()+" "+getMessage ();   // NOI18N
    }
}
