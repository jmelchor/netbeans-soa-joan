package org.netbeans.modules.sun.manager.jbi.util;

import java.io.File;
import org.netbeans.api.keyring.Keyring;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author David BRASSELY
 */
public class GlassfishServerInstanceFactory extends AbstractServerInstanceFactory {

    private final static String GLASSFISH_SERVER_DEFAULT_PASSWORD = "adminadmin"; // NOI18N
    private final static String DOMAIN = "DOMAIN"; // NOI18N
    private final static String HTTP_MONITOR_ON = "HttpMonitorOn"; // NOI18N
    private final static String HTTP_PORT_NUMBER = "httpportnumber"; // NOI18N
    private final static String LOCATION = "LOCATION"; // NOI18N
    private final static String DOMAINS_FOLDER = "domainsfolder"; // NOI18N
    private static final String SERVER_TARGET = "server";
    private final static String HOME_FOLDER = "homefolder"; // NOI18N
    
    @Override
    public boolean canHandle(String instanceDisplayName) {
        return instanceDisplayName.contains("GlassFish");
    }

    @Override
    public ServerInstance create(Node instanceNode, boolean withoutPassword) {
        NodeList childNodes = instanceNode.getChildNodes();

        GlassfishServerInstance instance = new GlassfishServerInstance();

        for (int j = 0; j < childNodes.getLength(); j++) {
            Node childNode = childNodes.item(j);
            String childNodeName = childNode.getNodeName();

            if ((childNode.getNodeType() == Node.ELEMENT_NODE)
                    && (childNodeName.equalsIgnoreCase("attr"))) { // NOI18N
                Element attrElement = (Element) childNode;

                String key = attrElement.getAttribute("name");  // NOI18N
                String value = attrElement.getAttribute("stringvalue"); // NOI18N

                if (key.equalsIgnoreCase(DISPLAY_NAME)) {
                    instance.setDisplayName(value);
                } else if (key.equalsIgnoreCase(DOMAIN)) {
                    instance.setDomain(value);
                } else if (key.equalsIgnoreCase(HTTP_MONITOR_ON)) {
                    instance.setHttpMonitorOn(value);
                } else if (key.equalsIgnoreCase(HTTP_PORT_NUMBER)) {
                    instance.setHttpPortNumber(value);
                } else if (key.equalsIgnoreCase(LOCATION) || key.equalsIgnoreCase(DOMAINS_FOLDER)) {
                    instance.setLocation(value);
                    if (key.equalsIgnoreCase(LOCATION)) {
                        instance.setVersion(GlassfishServerInstance.Version.GLASSFISH_V2);
                    } else {
                        instance.setVersion(GlassfishServerInstance.Version.GLASSFISH_V4);
                    }
                } else if (key.equalsIgnoreCase(PASSWORD)) {
                    instance.setPassword(value);
                } else if (key.equalsIgnoreCase(URL)) {
                    instance.setUrl(value);

                    String[] separator = value.split(":"); // NOI18N

                    int k = separator.length - 1;

                    /* NB5.0 Format changed...
                     [C:\alaska\root\jbi\runtime\Sun\AppServer]deployer:Sun:AppServer::localhost:4848
                     [/home/tli/SUNWappserver]deployer:Sun:AppServer::localhost:24848"
                     */
                    instance.setHostName(separator[k - 1]);
                    instance.setAdminPort(separator[k]);

                    /**
                     * Gets the location part from the URL. For remote server,
                     * the location is undefined. The location part from the URL
                     * gives the location of local server installation.
                     */
                    int index = value.indexOf(']');   // NOI18N
                    if (index != -1) {
                        instance.setUrlLocation(value.substring(1, index));
                        
                        int l = instance.getUrlLocation().indexOf(File.pathSeparatorChar);
                        if (l != -1) { // case of GFv3 / GFv4
                            instance.setUrlLocation(instance.getUrlLocation().substring(0, l));
                        }
                    }


                } else if (key.equalsIgnoreCase(USER_NAME)) {
                    instance.setUserName(value);
                }
            }
        }
        
        // By default, we use the server target
        instance.setTarget(SERVER_TARGET);

        if (instance.isGlassfish3or4x()) {
            instance.setAdminPort("8686");
        }
        
        if (instance.getHostName() != null && instance.getLocation() != null) {
            if (!withoutPassword) {
                if (instance.isGlassfish3or4x()) {
                    char[] pass = Keyring.read(passwordKey(
                            instance.getDisplayName(), instance.getUserName()));
                    if (pass != null) {
                        String p = new String(pass);
                        instance.setPassword(p);
                    } 
                } else {
                    char[] pass = Keyring.read(instance.getUrl());
                    if (pass != null) {
                        String p = new String(pass);
                        instance.setPassword(p);
                    } else {
                        instance.setPassword(GLASSFISH_SERVER_DEFAULT_PASSWORD);
                    }
                }
            }
            
            // If we do not have any username, no reason to have a password or a default one
            if (instance.getUserName() == null || instance.getUserName().isEmpty()) {
                instance.setPassword("");
            }

            return instance;
        }

        return null;
    }
    
    static final String KEYRING_NAME_SPACE="GlassFish.admin";
    static final String KEYRING_NAME_SEPARATOR=".";
    static final String KEYRING_IDENT_SEPARATOR=":";
    static final String PASSWORD_ATTR = "password"; // NOI18N
    
    public static String passwordKey(String serverName, String userName) {
        StringBuilder pwKey = new StringBuilder(
                KEYRING_NAME_SPACE.length() + KEYRING_NAME_SEPARATOR.length()
                + PASSWORD_ATTR.length()
                + KEYRING_IDENT_SEPARATOR.length()
                + (serverName != null ? serverName.length() : 0)
                + KEYRING_IDENT_SEPARATOR.length()
                + (userName != null ? userName.length() : 0));
        pwKey.append(KEYRING_NAME_SPACE);
        pwKey.append(KEYRING_NAME_SEPARATOR);
        pwKey.append(PASSWORD_ATTR);
        pwKey.append(KEYRING_IDENT_SEPARATOR);
        pwKey.append(serverName != null ? serverName : "");
        pwKey.append(KEYRING_IDENT_SEPARATOR);
        pwKey.append(userName != null ? userName : "");
        return pwKey.toString();
    }
}
