package org.netbeans.modules.sun.manager.jbi.util;

import org.w3c.dom.Node;

/**
 *
 * @author David BRASSELY
 */
public interface ServerInstanceFactory {
    
    boolean canHandle(Node instanceNode);
    
    ServerInstance create(Node instanceNode, boolean withoutPassword);
}
