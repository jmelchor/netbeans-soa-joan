package org.netbeans.modules.sun.manager.jbi.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import javax.management.MBeanServerConnection;
import org.netbeans.modules.sun.manager.jbi.management.JBIFrameworkService;

/**
 *
 * @author David BRASSELY
 */
public class GlassfishServerInstance extends ServerInstance {

    private Version version;

    @Override
    public List<File> getClasses() {
        List<File> classes = new ArrayList<File>();

        // Get the server location. This is not the domain or instance location. #90749
        String serverLocation = this.getUrlLocation();

        if (isGlassfish2x()) {
            String appserv_rt_jar = serverLocation + "/lib/appserv-rt.jar"; // NOI18N

            File appserv_rt_jarFile = new File(appserv_rt_jar);
            if (appserv_rt_jarFile.exists()) {
                classes.add(appserv_rt_jarFile);
            } else {
                throw new RuntimeException("JbiClassLoader: Cannot find " + appserv_rt_jar + ".");
            }

            String jbi_rt_jar_90 = serverLocation + "/addons/jbi/lib/jbi_rt.jar"; // NOI18N // Glassfish 9.0
            String jbi_rt_jar_91 = serverLocation + "/jbi/lib/jbi_rt.jar"; // NOI18N // Glassfish 9.1

            File jbi_rt_jarFile = new File(jbi_rt_jar_90);
            if (jbi_rt_jarFile.exists()) {
                classes.add(jbi_rt_jarFile);
            } else {
                jbi_rt_jarFile = new File(jbi_rt_jar_91);
                if (jbi_rt_jarFile.exists()) {
                    classes.add(jbi_rt_jarFile);
                } else {
                    throw new RuntimeException("JbiClassLoader: Cannot locate "
                            + jbi_rt_jar_90 + " or " + jbi_rt_jar_91 + ".");
                }
            }
        } else if (isGlassfish3or4x()) {
            File dir = new File(serverLocation + "/modules");
            File[] foundFiles = dir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.startsWith("openesb-gfv4");
                }
            });

            if (foundFiles.length == 0) {
                throw new RuntimeException("JbiClassLoader: Cannot find OpenESB for Glassfish v4 module.");
            } else {
                // Pick up only the first one : openesb-gfv4-framework-*.jar
                classes.add(foundFiles[0]);
            }
        }

        return classes;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public boolean isGlassfish2x() {
        return version == Version.GLASSFISH_V2;
    }

    public boolean isGlassfish3or4x() {
        return version == Version.GLASSFISH_V3 || version == Version.GLASSFISH_V4;
    }

    @Override
    public MBeanServerConnection getConnection() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isJBIEnabled(MBeanServerConnection mBeanServerConnection) {
        if (isGlassfish2x()) {
            JBIFrameworkService service
                    = new JBIFrameworkService(mBeanServerConnection);
            return service.isJbiFrameworkEnabled();
        } else {
            return true;
        }
    }

    enum Version {
        GLASSFISH_V2,
        GLASSFISH_V3,
        GLASSFISH_V4
    }
}
