package org.netbeans.modules.sun.manager.jbi.util;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author David BRASSELY
 */
public class JBossServerInstanceFactory extends AbstractServerInstanceFactory {

    private final static String JBOSS_DISPLAY_NAME = "OpenESB for JBoss Application Server";
    
    private final static String SERVER = "server"; // NOI18N
    private final static String HTTP_PORT_NUMBER = "port"; // NOI18N
    private final static String HOST = "host"; // NOI18N
    private final static String DEPLOY_DIR = "deploy-dir"; // NOI18N
    private final static String ROOT_DIR = "root-dir"; // NOI18N
    private final static String ADMIN_PORT_NUMBER = "admin-port"; // NOI18N
            
    @Override
    public boolean canHandle(String instanceDisplayName) {
        return JBOSS_DISPLAY_NAME.equalsIgnoreCase(instanceDisplayName);
    }
    
    @Override
    public ServerInstance create(Node instanceNode, boolean withoutPassword) {
        NodeList childNodes = instanceNode.getChildNodes();

        ServerInstance instance = new JBossServerInstance();

        for (int j = 0; j < childNodes.getLength(); j++) {
            Node childNode = childNodes.item(j);
            String childNodeName = childNode.getNodeName();

            if ((childNode.getNodeType() == Node.ELEMENT_NODE)
                    && (childNodeName.equalsIgnoreCase("attr"))) { // NOI18N
                Element attrElement = (Element) childNode;

                String key = attrElement.getAttribute("name");  // NOI18N
                String value = attrElement.getAttribute("stringvalue"); // NOI18N

                if (key.equalsIgnoreCase(DISPLAY_NAME)) {
                    instance.setDisplayName(value);
                } else if (key.equalsIgnoreCase(SERVER)) {
                    instance.setDomain(value);
                    instance.setTarget(value);
                } else if (key.equalsIgnoreCase(HTTP_PORT_NUMBER)) {
                    instance.setHttpPortNumber(value);
                } else if (key.equalsIgnoreCase(DEPLOY_DIR)) {
                    instance.setLocation(value);
                } else if (key.equalsIgnoreCase(URL)) {
                    instance.setUrl(value);
                } else if (key.equalsIgnoreCase(USER_NAME)) {
                    instance.setUserName(value);
                } else if (key.equalsIgnoreCase(PASSWORD)) {
                    instance.setPassword(value);
                } else if (key.equalsIgnoreCase(ROOT_DIR)) {
                    instance.setUrlLocation(value);
                } else if (key.equalsIgnoreCase(ADMIN_PORT_NUMBER)) {
                    instance.setAdminPort(value);
                } else if (key.equalsIgnoreCase(HOST)) {
                    instance.setHostName(value);
                }
            }
        }
        
        return instance;
    }
}
