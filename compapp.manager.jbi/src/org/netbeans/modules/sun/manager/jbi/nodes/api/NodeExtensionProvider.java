package org.netbeans.modules.sun.manager.jbi.nodes.api;

import javax.management.MBeanServerConnection;
import org.openide.nodes.Node;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface NodeExtensionProvider {
    
    /**
     * Gets the JBI node under the application server node.
     * 
     * @param connection 
     * 
     * @return  the top level JBI meta-container node if the "JBIFramework"  
     *          Lifecycle Module is installed in the application server; 
     *          or <code>null</code> otherwise.
     */
    public Node getExtensionNode(MBeanServerConnection connection);
}
