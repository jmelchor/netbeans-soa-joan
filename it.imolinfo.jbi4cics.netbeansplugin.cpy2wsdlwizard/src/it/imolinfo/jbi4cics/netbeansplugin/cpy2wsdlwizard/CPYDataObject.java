/*
 *  Copyright (c) 2005, 2006 Imola Informatica.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the LGPL License v2.1
 *  which accompanies this distribution, and is available at
 *  http://www.gnu.org/licenses/lgpl.html
 */


package it.imolinfo.jbi4cics.netbeansplugin.cpy2wsdlwizard;

import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataNode;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.text.DataEditorSupport;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;

public class CPYDataObject extends MultiDataObject {

    private static final String MIME_TYPE = "text/x-cpy";       // NOI18N

    public CPYDataObject(final FileObject pf, MultiFileLoader loader)
            throws IOException {
        super(pf, loader);
        registerEditor(MIME_TYPE, true);     // NOI18N
    }

    @Override
    protected Node createNodeDelegate() {
        return new DataNode(this, Children.LEAF, getLookup());
    }
    
    @Override
    protected int associateLookup() {
        return 1;
    }
    
    @Messages("Source=&Source")                                 // NOI18N
    @MultiViewElement.Registration(
            displayName="#Source",                              // NOI18N
            iconBase="it/imolinfo/jbi4cics/netbeansplugin/cpy2wsdlwizard/cpy16x16.png",     // NOI18N
            persistenceType=TopComponent.PERSISTENCE_ONLY_OPENED,
            mimeType=MIME_TYPE,
            preferredID="cpy.source",                           // NOI18N
            position=1
    )
    public static MultiViewEditorElement createMultiViewEditorElement(Lookup context) {
        return new MultiViewEditorElement(context);
    }

}
