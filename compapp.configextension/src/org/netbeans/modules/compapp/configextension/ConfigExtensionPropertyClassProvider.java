package org.netbeans.modules.compapp.configextension;

import org.netbeans.modules.compapp.casaeditor.properties.spi.ExtensionPropertyClassProvider;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author ads
 */
@ServiceProvider(service=ExtensionPropertyClassProvider.class)
public class ConfigExtensionPropertyClassProvider extends ExtensionPropertyClassProvider {
    
}
