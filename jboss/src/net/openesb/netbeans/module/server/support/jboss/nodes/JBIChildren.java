package net.openesb.netbeans.module.server.support.jboss.nodes;

import java.io.File;
import java.io.IOException;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.deploy.spi.DeploymentManager;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import net.openesb.netbeans.module.server.support.jboss.nodes.actions.Refreshable;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory;
import org.netbeans.modules.j2ee.jboss4.JBDeploymentManager;
import org.netbeans.modules.j2ee.jboss4.JBoss5ProfileServiceProxy;
import org.netbeans.modules.j2ee.jboss4.ide.ui.JBPluginProperties;
import org.netbeans.modules.j2ee.jboss4.ide.ui.JBPluginUtils;
import org.netbeans.modules.j2ee.jboss4.util.JBProperties;
import org.netbeans.modules.sun.manager.jbi.nodes.api.NodeExtensionProvider;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class JBIChildren extends JBIAsyncChildren implements Refreshable {

    private static final Logger LOGGER = Logger.getLogger(JBIChildren.class.getName());
    public static final String WAIT_NODE = "wait_node"; //NOI18N
    
    private final Lookup lookup;

    JBIChildren(Lookup lookup) {
	this.lookup = lookup;
    }

    @Override
    public void updateKeys() {
	setKeys(new Object[]{createWaitNode()});
	getExecutorService().submit(new JBINodeUpdater(), 0);
    }
    
    /* Creates and returns the instance of the node
     * representing the status 'WAIT' of the node.
     * It is used when it spent more time to create elements hierarchy.
     * @return the wait node.
     */
    private Node createWaitNode() {
        AbstractNode n = new AbstractNode(Children.LEAF);
        n.setName(NbBundle.getMessage(JBIItemNode.class, "LBL_WaitNode_DisplayName")); //NOI18N
        n.setIconBaseWithExtension("net/openesb/netbeans/module/server/support/jboss/resources/wait.gif"); // NOI18N
        return n;
    }

    class JBINodeUpdater implements Runnable {

	List keys = new ArrayList();

	@Override
	public void run() {
            
	    //StandaloneDeploymentManager sdm = lookup.lookup(StandaloneDeploymentManager.class);
	    //if (sdm.getMBeanServerConnection() != null) {
		for (NodeExtensionProvider nep : Lookup.getDefault().lookupAll(NodeExtensionProvider.class)) {
		    if (nep != null) {
			Node node = nep.getExtensionNode(getMBeanServerConnection(lookup));
			if (node != null) {
			    keys.add(node);
			}
		    }
		}

	    setKeys(keys);
	}
        
        private MBeanServerConnection getMBeanServerConnection(Lookup lookup) {
            JBDeploymentManager dm = (JBDeploymentManager) lookup.lookup(DeploymentManager.class);

            ClassLoader oldLoader = Thread.currentThread().getContextClassLoader();
            InitialContext ctx = null;
            JMXConnector conn = null;

            try {
                InstanceProperties ip = dm.getInstanceProperties();
                URLClassLoader loader = JBDeploymentFactory.getInstance().getJBClassLoader(ip);
                Thread.currentThread().setContextClassLoader(loader);

                JBProperties props = dm.getProperties();
                Properties env = new Properties();

                // Sets the jboss naming environment
                String jnpPort = Integer.toString(
                        JBPluginUtils.getJnpPortNumber(ip.getProperty(JBPluginProperties.PROPERTY_SERVER_DIR)));

                env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.NamingContextFactory");
                env.put(Context.PROVIDER_URL, "jnp://localhost"+ ":"  + jnpPort);
                env.put(Context.OBJECT_FACTORIES, "org.jboss.naming");
                env.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces" );
                env.put("jnp.disableDiscovery", Boolean.TRUE);

                final String JAVA_SEC_AUTH_LOGIN_CONF = "java.security.auth.login.config"; // NOI18N
                String oldAuthConf = System.getProperty(JAVA_SEC_AUTH_LOGIN_CONF);

                env.put(Context.SECURITY_PRINCIPAL, props.getUsername());
                env.put(Context.SECURITY_CREDENTIALS, props.getPassword());
                env.put("jmx.remote.credentials", // NOI18N
                        new String[] {props.getUsername(), props.getPassword()});

                File securityConf = new File(props.getRootDir(), "/client/auth.conf");
                if (securityConf.exists()) {
                    env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.security.jndi.LoginInitialContextFactory");
                    System.setProperty(JAVA_SEC_AUTH_LOGIN_CONF, securityConf.getAbsolutePath()); // NOI18N
                }

                if (!props.isVersion(JBPluginUtils.JBOSS_7_0_0)) {
                        // Gets naming context
                        ctx = new InitialContext(env);
                    }

                //restore java.security.auth.login.config system property
                if (oldAuthConf != null) {
                    System.setProperty(JAVA_SEC_AUTH_LOGIN_CONF, oldAuthConf);
                } else {
                    System.clearProperty(JAVA_SEC_AUTH_LOGIN_CONF);
                }

                MBeanServerConnection rmiServer = null;
                    try {
                        JMXServiceURL url;
                        if (props.isVersion(JBPluginUtils.JBOSS_7_0_0)) {
                            // using management-native port
                            url = new JMXServiceURL(
                                    System.getProperty("jmx.service.url", "service:jmx:remoting-jmx://localhost:9999")); // NOI18N
                        } else {
                            url = new JMXServiceURL(
                                    "service:jmx:rmi:///jndi/rmi://localhost:1090/jmxrmi"); // NOI18N
                        }
                        conn = JMXConnectorFactory.connect(url);

                        rmiServer = conn.getMBeanServerConnection();
                    } catch (IOException ex) {
                        LOGGER.log(Level.FINE, null, ex);
                    }

                    if (rmiServer == null && ctx != null) {
                        // Lookup RMI Adaptor
                        rmiServer = (MBeanServerConnection) ctx.lookup("/jmx/invoker/RMIAdaptor"); // NOI18N
                    }

                    JBoss5ProfileServiceProxy profileService = null;
                    try {
                        if (ctx != null) {
                            Object service = ctx.lookup("ProfileService"); // NOI18N
                            if (service != null) {
                                profileService = new JBoss5ProfileServiceProxy(service);
                            }
                        }
                    } catch (NameNotFoundException ex) {
                        LOGGER.log(Level.FINE, null, ex);
                    }

                return rmiServer;
            } catch (NameNotFoundException ex) {
                LOGGER.log(Level.FINE, null, ex);
                return null;
            } catch (NamingException ex) {
                LOGGER.log(Level.FINE, null, ex);
                return null;
            } catch (Exception ex) {
                LOGGER.log(Level.FINE, null, ex);
                return null;
            } finally {
                Thread.currentThread().setContextClassLoader(oldLoader);
            }
        }
    }
    

    @Override
    protected void addNotify() {
	updateKeys();
    }

    @Override
    protected void removeNotify() {
	setKeys(java.util.Collections.EMPTY_SET);
    }

    @Override
    protected org.openide.nodes.Node[] createNodes(Object key) {
	if (key instanceof Node) {
	    return new Node[]{(Node) key};
	}

	if (key instanceof String && key.equals(WAIT_NODE)) {
	    return new Node[]{createWaitNode()};
	}

	return null;
    }
}
