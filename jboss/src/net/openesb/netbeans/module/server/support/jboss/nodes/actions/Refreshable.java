package net.openesb.netbeans.module.server.support.jboss.nodes.actions;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface Refreshable {

    public void updateKeys();
}
