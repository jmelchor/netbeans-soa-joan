package net.openesb.netbeans.module.server.support.jboss.ide;

import javax.enterprise.deploy.spi.DeploymentManager;
import net.openesb.netbeans.module.server.support.jboss.ide.ui.JBInstantiatingIterator;
import org.netbeans.modules.j2ee.deployment.plugins.spi.DatasourceManager;
import org.netbeans.modules.j2ee.deployment.plugins.spi.FindJSPServlet;
import org.netbeans.modules.j2ee.deployment.plugins.spi.IncrementalDeployment;
import org.netbeans.modules.j2ee.deployment.plugins.spi.JDBCDriverDeployer;
import org.netbeans.modules.j2ee.deployment.plugins.spi.MessageDestinationDeployment;
import org.netbeans.modules.j2ee.deployment.plugins.spi.OptionalDeploymentManagerFactory;
import org.netbeans.modules.j2ee.deployment.plugins.spi.ServerInstanceDescriptor;
import org.netbeans.modules.j2ee.deployment.plugins.spi.StartServer;
import org.openide.WizardDescriptor;

/**
 *
 * @author David BRASSELY
 */
public class JBOptionalDeploymentManagerFactory extends org.netbeans.modules.j2ee.jboss4.ide.JBOptionalDeploymentManagerFactory {
    
    /*
    private OptionalDeploymentManagerFactory super;
    private static JBOptionalDeploymentManagerFactory instance;
    
    public static synchronized OptionalDeploymentManagerFactory create() throws Exception {
        if (instance == null) {
            Class<?> clazz = Class.forName("org.netbeans.modules.j2ee.jboss4.ide.JBOptionalDeploymentManagerFactory");
            instance = new JBOptionalDeploymentManagerFactory();
            instance.setOptionalDeploymentManagerFactory((OptionalDeploymentManagerFactory) clazz.newInstance());
        }
        
        return instance;
    }
    
    private JBOptionalDeploymentManagerFactory() {
    }
    
    private void setOptionalDeploymentManagerFactory(OptionalDeploymentManagerFactory wrappedOptionalDeploymentManagerFactory) {
        this.super = wrappedOptionalDeploymentManagerFactory;
    }
    */
    
    @Override
    public StartServer getStartServer(DeploymentManager dm) {
        return super.getStartServer(dm);
    }
    
    @Override
    public IncrementalDeployment getIncrementalDeployment(DeploymentManager dm) {
        return super.getIncrementalDeployment(dm);
    }
    
    @Override
    public FindJSPServlet getFindJSPServlet(DeploymentManager dm) {
        return super.getFindJSPServlet(dm);
    }
    
    @Override
    public WizardDescriptor.InstantiatingIterator getAddInstanceIterator() {
        return new JBInstantiatingIterator();
    }
    
    @Override
    public DatasourceManager getDatasourceManager(DeploymentManager dm) {
        return super.getDatasourceManager(dm);
    }
    
    @Override
    public MessageDestinationDeployment getMessageDestinationDeployment(DeploymentManager dm) {
        return super.getMessageDestinationDeployment(dm);
    }
    
    @Override
    public JDBCDriverDeployer getJDBCDriverDeployer(DeploymentManager dm) {
        return super.getJDBCDriverDeployer(dm);
    }
    
    @Override
    public ServerInstanceDescriptor getServerInstanceDescriptor(DeploymentManager dm) {
        return super.getServerInstanceDescriptor(dm);
    }
}