package net.openesb.netbeans.module.server.support.jboss.nodes;

import net.openesb.netbeans.module.server.support.jboss.nodes.actions.RefreshAction;
import net.openesb.netbeans.module.server.support.jboss.nodes.actions.RefreshCookie;
import net.openesb.netbeans.module.server.support.jboss.nodes.actions.Refreshable;
import java.awt.Image;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.actions.SystemAction;

/**
 * Default Node which can have refresh action enabled and which has default icon.
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class JBIItemNode extends AbstractNode {
    
    public JBIItemNode(Children children, String name){
        super(children);
        setDisplayName(name);
        if(getChildren() instanceof Refreshable)
            getCookieSet().add(new RefreshCookieImpl((Refreshable)getChildren()));
    }
    
    @Override
    public Image getIcon(int type) {
	return getIconDelegate().getIcon(type);
    }
    
    @Override
    public Image getOpenedIcon(int type) {
	return getIconDelegate().getOpenedIcon(type);
    }
    
    private Node getIconDelegate() {
        return DataFolder.findFolder(FileUtil.getConfigRoot()).getNodeDelegate();
    }
    
    @Override
    public javax.swing.Action[] getActions(boolean context) {
        if(getChildren() instanceof Refreshable)
            return new SystemAction[] {
                SystemAction.get(RefreshAction.class)
            };
        
        return new SystemAction[] {};
    }
    
    /**
     * Implementation of the RefreshCookie
     */
    private static class RefreshCookieImpl implements RefreshCookie {
        Refreshable children;
        
        public RefreshCookieImpl(Refreshable children){
            this.children = children;
        }
        
	@Override
        public void refresh() {
            children.updateKeys();
        }
    }
}
