package net.openesb.netbeans.module.server.support.jboss.nodes;

import java.util.concurrent.ExecutorService;
import org.openide.nodes.Children;
import org.openide.util.RequestProcessor;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class JBIAsyncChildren extends Children.Keys {

    private static final ExecutorService EXECUTOR = new RequestProcessor(JBIAsyncChildren.class);

    public final ExecutorService getExecutorService() {
        return EXECUTOR;
    }
}
