package net.openesb.netbeans.module.server.support.jboss.ide.ui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.openesb.netbeans.module.server.support.jboss.JBDeploymentFactory;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.netbeans.modules.j2ee.jboss4.ide.ui.JBPluginProperties;
import org.netbeans.modules.j2ee.jboss4.ide.ui.JBPluginUtils;

/**
 *
 * @author David BRASSELY
 */
public class JBInstantiatingIterator extends org.netbeans.modules.j2ee.jboss4.ide.ui.JBInstantiatingIterator {

    private static final String JBOSS_OPENESB_JAVA_OPTS = "-Xms1024m -Xmx1024m -XX:MaxPermSize=256m"; // NOI18N

    @Override
    public Set instantiate() throws IOException {
        Set result = super.instantiate();

        if (!result.isEmpty()) {
            InstanceProperties ip = (InstanceProperties) result.iterator().next();
            
            // Clone properties before removing initial instance
            Map<String, String> initialProperties = cloneInitialProperties(ip);
            
            // Clone required properties
            String oldUrl = ip.getProperty(InstanceProperties.URL_ATTR);
            String username = ip.getProperty(InstanceProperties.USERNAME_ATTR);
            String password = ip.getProperty(InstanceProperties.PASSWORD_ATTR);
            String displayName = ip.getProperty(InstanceProperties.DISPLAY_NAME_ATTR);
            
            String domainDir = ip.getProperty(JBPluginProperties.PROPERTY_SERVER_DIR);
            
            // Set RMI admin port
            // TODO: Find a way to retrieve automatically from jboss-service.xml ?
            initialProperties.put("admin-port", "1090");
            
            // Override JAVA_OPTS for OpenESB runtime
            initialProperties.put(JBPluginProperties.PROPERTY_JAVA_OPTS, 
                    JBOSS_OPENESB_JAVA_OPTS);
            
            String enhancedUrl = oldUrl.replaceFirst(
                    org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory.URI_PREFIX, 
                    JBDeploymentFactory.URI_PREFIX);

            InstanceProperties newIP = InstanceProperties.createInstanceProperties(
                    enhancedUrl, username, password, displayName, initialProperties);
            
            // Clear previous instance
            result.clear();
            
            // Remove the old instance created using the old url
            InstanceProperties.removeInstance(oldUrl);
            
            // Add the new OpenESB instance
            result.add(newIP);
        }

        return result;
    }

    private Map<String, String> cloneInitialProperties(InstanceProperties ip) {
        Map<String, String> initialProperties = new HashMap<String, String>(7);
        
        initialProperties.put(JBPluginProperties.PROPERTY_SERVER, 
                ip.getProperty(JBPluginProperties.PROPERTY_SERVER));
        initialProperties.put(JBPluginProperties.PROPERTY_DEPLOY_DIR, 
                ip.getProperty(JBPluginProperties.PROPERTY_DEPLOY_DIR));
        initialProperties.put(JBPluginProperties.PROPERTY_SERVER_DIR, 
                ip.getProperty(JBPluginProperties.PROPERTY_SERVER_DIR));
        initialProperties.put(JBPluginProperties.PROPERTY_ROOT_DIR, 
                ip.getProperty(JBPluginProperties.PROPERTY_ROOT_DIR));
        initialProperties.put(JBPluginProperties.PROPERTY_HOST, 
                ip.getProperty(JBPluginProperties.PROPERTY_HOST));
        initialProperties.put(JBPluginProperties.PROPERTY_PORT, 
                ip.getProperty(JBPluginProperties.PROPERTY_PORT));
        /*
         * We are using specific JAVA_OPTS for OpenESB runtime
        initialProperties.put(JBPluginProperties.PROPERTY_JAVA_OPTS, 
                ip.getProperty(JBPluginProperties.PROPERTY_JAVA_OPTS));
        */
        return initialProperties;
    }
}
