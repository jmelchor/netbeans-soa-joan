package org.netbeans.modules.wsdlextensions.ldap.ldif;

import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.text.DataEditorSupport;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;

public class LdifDataObject extends MultiDataObject implements Lookup.Provider {

    public LdifDataObject(FileObject pf, LdifDataLoader loader) 
        throws DataObjectExistsException, IOException 
    {
        super(pf, loader);
        registerEditor(LdifDataLoader.REQUIRED_MIME, true);     
    }
    
    protected Node createNodeDelegate() {
        return new LdifDataNode(this, getLookup());
    }

    @Override
    protected int associateLookup() {
        return 1;
    }
    
    @Messages("Source=&Source")                                 // NOI18N
    @MultiViewElement.Registration(
            displayName="#Source",                              // NOI18N
            iconBase=LdifDataNode.IMAGE_ICON_BASE,     
            persistenceType=TopComponent.PERSISTENCE_ONLY_OPENED,
            mimeType=LdifDataLoader.REQUIRED_MIME,
            preferredID="ldif.source",                           // NOI18N
            position=1
    )
    public static MultiViewEditorElement createMultiViewEditorElement(Lookup context) {
        return new MultiViewEditorElement(context);
    }
}
