# README #

This readme explain how to use the code to generate the OpenESB Studio with the current code.  

## What does I need ##

The OpenESB Studio can be generated with ANT, but the simplest way is to use a Netbeans or an OpenESB Studio
We recommand (18S1) to use Netbeans 8.2. Netbeans can be found on the www.netbeans.org

## How to do build openESB Studio ? ###

### Step 01 get the code ###
Use Git to download the code. Netbeans can be used to do it but we found safer to use git directly

### Step 02 Open the project ###
In netbeans, Open the main project netbeans-soa

### Step 03 Clean the project ###
Clean the project netbeans-soa. This command downloads many jar file from the Netbeans platform.  
It could take more that 15 minutes

### Step 04 Build Run Debug the project ###
From now, you can run debug the project but also create a new package such as an windows or linux installer.
You can also create NBMs or a Zip file that contains all you the exe and libs required to run OpenESB Studio

## Need help ##
Please register to the openESB user list or on the OpenESB Nabble forum  

## Push Request ##
Please feel free to send your push request 
