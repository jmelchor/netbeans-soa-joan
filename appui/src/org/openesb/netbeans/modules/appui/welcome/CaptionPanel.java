package org.openesb.netbeans.modules.appui.welcome;

import java.awt.BorderLayout;
import java.awt.Image;
import javax.swing.JPanel;
import org.openide.util.ImageUtilities;

/**
 *
 * @author David BRASSELY
 */
class CaptionPanel extends JPanel {
    
    private static final String TOP_LEFT_RESOURCE =   "org/openesb/netbeans/modules/appui/welcome/resources/welcome-topleft.png";
    private static final String TOP_RIGHT_RESOURCE =  "org/openesb/netbeans/modules/appui/welcome/resources/welcome-topright.png";
    private static final String TOP_MIDDLE_RESOURCE = "org/openesb/netbeans/modules/appui/welcome/resources/welcome-topmiddle.png";
    
    
    public CaptionPanel() {
        initComponents();
    }
    
    
    private void initComponents() {
        setLayout(new BorderLayout());
        setOpaque(false);
        
        Image topLeftImage = ImageUtilities.loadImage(TOP_LEFT_RESOURCE, true);
        Image topRightImage = ImageUtilities.loadImage(TOP_RIGHT_RESOURCE, true);
        Image topMiddleImage = ImageUtilities.loadImage(TOP_MIDDLE_RESOURCE, true);
        
        add(new FixedImagePanel(topLeftImage), BorderLayout.WEST);
        add(new FixedImagePanel(topRightImage), BorderLayout.EAST);
        add(new HorizontalImagePanel(topMiddleImage), BorderLayout.CENTER);
    }

}
