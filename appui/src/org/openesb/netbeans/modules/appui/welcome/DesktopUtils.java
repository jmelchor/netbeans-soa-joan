package org.openesb.netbeans.modules.appui.welcome;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * Support for java.awt.Desktop
 *
 * @author David BRASSELY
 */
public final class DesktopUtils {

    private static final Object allowDesktopOpen = System.getProperty("com.sun.tools.visualvm.core.ui.DesktopUtils.allowDesktopOpen");  // NOI18N

    private static boolean desktopSupported;
    private static boolean desktopSupportedInitialized = false;
    private static boolean browseAvailable;
    private static boolean browseAvailableInitialized = false;
    private static boolean openAvailable;
    private static boolean openAvailableInitialized = false;


    /**
     * Returns true if java.awt.Desktop is supported on host platform.
     * 
     * @return true if java.awt.Desktop is supported on host platform, false otherwise.
     */
    public synchronized static boolean isDesktopSupported() {
        if (!desktopSupportedInitialized) {
            desktopSupported = Desktop.isDesktopSupported();
            desktopSupportedInitialized = true;
        }
        return desktopSupported;
    }
    
    // NOTE: should always return true if in NetBeans IDE (uses its own user-customizable mechanism)
    /**
     * Returns true if opening a www address in a web browser is supported on host platform.
     * 
     * @return true if opening a www address in a web browser is supported on host platform, false otherwise.
     */
    public synchronized static boolean isBrowseAvailable() {
        if (!browseAvailableInitialized) {
            if (!isDesktopSupported()) browseAvailable = false;
            else browseAvailable = Desktop.getDesktop().isSupported(Desktop.Action.BROWSE);
            browseAvailableInitialized = true;
        }
        return browseAvailable;
    }
    
    /**
     * Returns true if opening a file in native displayer is supported on host platform.
     * 
     * @return true if opening a file in native displayer is supported on host platform, false otherwise.
     */
    public synchronized static boolean isOpenAvailable() {
        if (!openAvailableInitialized) {
             // NOTE: Open is disabled by default, crashes the VM at least on Windows
            if ("true".equals(allowDesktopOpen) && isDesktopSupported()) openAvailable = Desktop.getDesktop().isSupported(Desktop.Action.OPEN); // NOI18N
            else openAvailable = false;
            openAvailableInitialized = true;
        }
        return openAvailable;
    }
    
    // NOTE: If in NetBeans IDE use HtmlBrowser.URLDisplayer.showURL(URL u)
    /**
     * Performs Desktop.getDesktop().browse(URI).
     * 
     * @param uri URI to open.
     * @throws java.io.IOException
     */
    public static void browse(URI uri) throws IOException {
        Desktop.getDesktop().browse(uri);
    }
    
    /**
     * Performs Desktop.getDesktop().open(File).
     * 
     * @param file File to open.
     * @throws java.io.IOException
     */
    public static void open(File file) throws IOException {
        Desktop.getDesktop().open(file);
    }

}
