package org.openesb.netbeans.modules.appui.welcome;

import java.util.Set;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;
import org.openide.windows.TopComponent;

/**
 * Show the welcome screen.
 * @author  David BRASSELY
 */
public class ShowWelcomeAction extends CallableSystemAction {

    public ShowWelcomeAction() {
        putValue("noIconInMenu", Boolean.TRUE); // NOI18N
    }

    public void performAction() {
        WelcomeComponent topComp = null;
        Set<TopComponent> tcs = TopComponent.getRegistry().getOpened();
        for (TopComponent tc: tcs) {
            if (tc instanceof WelcomeComponent) {                
                topComp = (WelcomeComponent) tc;               
                break;
            }
        }
        if(topComp == null){            
            topComp = WelcomeComponent.findComp();
        }
       
        topComp.open();
        topComp.requestActive();
    }
    
    public String getName() {
        return NbBundle.getMessage(ShowWelcomeAction.class, "LBL_Action"); //NOI18N
    }
    
    @Override protected String iconResource() {
        return "org/openesb/netbeans/modules/appui/welcome/resources/welcome.gif";  //NOI18N
    }
    
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
    
    @Override protected boolean asynchronous(){
        return false;
    }
}
