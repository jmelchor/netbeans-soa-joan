/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.modules.bpel.navigator;

import java.beans.PropertyChangeEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.netbeans.modules.bpel.model.api.events.ArrayUpdateEvent;
import org.netbeans.modules.bpel.model.api.events.EntityInsertEvent;
import org.netbeans.modules.bpel.model.api.events.EntityRemoveEvent;
import org.netbeans.modules.bpel.model.api.events.EntityUpdateEvent;
import org.netbeans.modules.bpel.model.api.events.PropertyRemoveEvent;
import org.netbeans.modules.bpel.model.api.events.PropertyUpdateEvent;
import org.openide.explorer.view.BeanTreeView;

/**
 *
 * @author polperez
 */
public class BpelModelLogicalBeanTreeTest {
    
    public BpelModelLogicalBeanTreeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of removeListeners method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testRemoveListeners() {
        System.out.println("removeListeners");
        BpelModelLogicalBeanTree instance = null;
        instance.removeListeners();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBeanTreeView method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testGetBeanTreeView() {
        System.out.println("getBeanTreeView");
        BpelModelLogicalBeanTree instance = null;
        BeanTreeView expResult = null;
        BeanTreeView result = instance.getBeanTreeView();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of propertyChange method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testPropertyChange() {
        System.out.println("propertyChange");
        PropertyChangeEvent evt = null;
        BpelModelLogicalBeanTree instance = null;
        instance.propertyChange(evt);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of notifyPropertyRemoved method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testNotifyPropertyRemoved() {
        System.out.println("notifyPropertyRemoved");
        PropertyRemoveEvent event = null;
        BpelModelLogicalBeanTree instance = null;
        instance.notifyPropertyRemoved(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of notifyEntityInserted method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testNotifyEntityInserted() {
        System.out.println("notifyEntityInserted");
        EntityInsertEvent event = null;
        BpelModelLogicalBeanTree instance = null;
        instance.notifyEntityInserted(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of notifyPropertyUpdated method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testNotifyPropertyUpdated() {
        System.out.println("notifyPropertyUpdated");
        PropertyUpdateEvent event = null;
        BpelModelLogicalBeanTree instance = null;
        instance.notifyPropertyUpdated(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of notifyEntityRemoved method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testNotifyEntityRemoved() {
        System.out.println("notifyEntityRemoved");
        EntityRemoveEvent event = null;
        BpelModelLogicalBeanTree instance = null;
        instance.notifyEntityRemoved(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of notifyEntityUpdated method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testNotifyEntityUpdated() {
        System.out.println("notifyEntityUpdated");
        EntityUpdateEvent event = null;
        BpelModelLogicalBeanTree instance = null;
        instance.notifyEntityUpdated(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of notifyArrayUpdated method, of class BpelModelLogicalBeanTree.
     */
    @Test
    public void testNotifyArrayUpdated() {
        System.out.println("notifyArrayUpdated");
        ArrayUpdateEvent event = null;
        BpelModelLogicalBeanTree instance = null;
        instance.notifyArrayUpdated(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
