package org.netbeans.modules.compapp.casaeditor.nodes.actions;

import java.io.File;
import java.io.IOException;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.netbeans.modules.compapp.casaeditor.CasaDataEditorSupport;
import org.netbeans.modules.compapp.casaeditor.CasaDataNode;
import org.netbeans.modules.compapp.casaeditor.CasaDataObject;
import org.netbeans.modules.compapp.casaeditor.model.casa.CasaEndpointRef;
import org.netbeans.modules.compapp.casaeditor.model.casa.CasaServiceEngineServiceUnit;
import org.netbeans.modules.compapp.casaeditor.model.casa.CasaWrapperModel;
import org.netbeans.modules.compapp.casaeditor.nodes.CasaNode;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * This action creates an external SU 
 * and add to it the endpoints extracted from the imported endPoints.xml
 * @author DavidD
 * not used currently
 */
public class AddImportedEndpoints extends NodeAction{
    
    @Override
    protected boolean enable(Node[] activatedNodes) {
        return true;
    }
    
    @Override
    protected boolean asynchronous() {
        return false;
    }
    
    @Override
    public String getName() {
        return NbBundle.getMessage(AddImportedEndpoints.class, "LBL_AddImportedEndpoints_Name"); // NOI18N
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public Action getAction() {
        return this;
    }
    
    @Override
    public void performAction(Node[] activatedNodes) {      
        
        //copied from AddExternalServiceUnitAction
        if (activatedNodes.length < 1) {
            return;
        }
        CasaWrapperModel cmodel = null;
        if (activatedNodes[0] instanceof CasaDataNode) {
            final CasaDataNode node = ((CasaDataNode) activatedNodes[0]);
            CasaDataObject obj = (CasaDataObject) node.getDataObject();
            CasaDataEditorSupport es = obj.getLookup().lookup(CasaDataEditorSupport.class);
            if (es != null) {
                cmodel = es.getModel();
            }
        } else if (activatedNodes[0] instanceof CasaNode) {
            final CasaNode node = ((CasaNode) activatedNodes[0]);
            cmodel = node.getModel();
        }

        if (cmodel == null) {
            return;
        }

        final CasaWrapperModel model = cmodel;
        
        //by DavidD, get the endpoints
        String projectPath = model.getJBIProject().getProjectDirectory().getPath();
        String endPointsXMLpath = projectPath+File.separator+"src"+File.separator+"conf"+File.separator+ImportServiceUnitFromEndPointAction.IMPORTED_ENDPOINTS_FILE;
        File fXmlFile = new File(endPointsXMLpath);
        if(!(fXmlFile.exists() && !fXmlFile.isDirectory())){
            //the file does not exist yet => no endPoints are available for importation
            //TODO handle failed use case
            return;
        }
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            //test : create the first provider
            Element provider = (Element) doc.getElementsByTagName(ImportServiceUnitFromEndPointAction.PROVIDERS_NODE).item(0);
            Element firstEP = (Element) provider.getElementsByTagName(ImportServiceUnitFromEndPointAction.ENDPOINT_NODE).item(0);
            Element name = (Element) firstEP.getElementsByTagName(ImportServiceUnitFromEndPointAction.ENDPOINT_NAME_NODE).item(0);
            final String endpointName = name.getTextContent();
            SwingUtilities.invokeLater(new Runnable(){
                @Override
                public void run(){
                    CasaServiceEngineServiceUnit SU = model.addServiceEngineServiceUnitFromPalette(false, 40, 40, null, null, null, null);//TODO set
                    CasaEndpointRef endPoint = model.addExternalEndpoint(SU, false);
                    try{
                        if(!model.startTransaction()){
                            throw new Exception("The transaction could not be started");
                        }else{
                            endPoint.getEndpoint().get().setEndpointName(endpointName);
                            model.endTransaction();
                    }     
                    }catch(Exception ex){
                        ex.printStackTrace();
                        //TODO action on fail
                    }
                }
            });
            //TODO actions on fail
        }catch(SAXException ex){
            ex.printStackTrace();
        }catch(IOException ex){
            ex.printStackTrace();                    
        }catch(ParserConfigurationException ex){
            ex.printStackTrace();
        }
    }   
}
