/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.modules.compapp.casaeditor.nodes.actions;

import javax.xml.namespace.QName;

/**
 *
 * @author DavidD
 */
public class ImportedEndPoint implements Comparable<ImportedEndPoint>{
    
    private boolean consumer;
    private String endPointName;
    private QName interfaceName;
    private QName serviceName;    
    private String projectDescription;
    private String projectName;
    private String projectVersion;
    private String teamName;
    private String teamContact;
    /**
     * 04/05/18 
     * Add the nickName property
     * see Jira ESBNBM-94
     */
    private String nickName;


    public ImportedEndPoint(boolean consumer, String endPointName, QName interfaceName, QName serviceName, String projectDesription, String projectName, String projectVersion, String teamName, String teamContact) {
        this.consumer = consumer;
        this.endPointName = endPointName;
        this.interfaceName = interfaceName;
        this.serviceName = serviceName;
        this.projectDescription = projectDesription;
        this.projectName = projectName;
        this.projectVersion = projectVersion;
        this.teamName = teamName;
        this.teamContact = teamContact;
        // The nick name is the local part of the service name. So there is no setNickname method here 
        this.nickName = this.serviceName.getLocalPart();
    }

    public boolean isConsumer() {
        return consumer;
    }

    public void setConsumer(boolean consumer) {
        this.consumer = consumer;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    public QName getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(QName interfaceName) {
        this.interfaceName = interfaceName;
    }

    public QName getServiceName() {
        return serviceName;
    }

    public void setServiceName(QName serviceName) {
        this.serviceName = serviceName;       
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectVersion() {
        return projectVersion;
    }

    public void setProjectVersion(String projectVersion) {
        this.projectVersion = projectVersion;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamContact() {
        return teamContact;
    }

    public void setTeamContact(String teamContact) {
        this.teamContact = teamContact;
    }
    
     public String getNickName() {
        return nickName;
    }
        
    @Override
    public int compareTo(ImportedEndPoint o) {
        return getEndPointName().compareToIgnoreCase(o.getEndPointName()); //To change body of generated methods, choose Tools | Templates.
    }
}
