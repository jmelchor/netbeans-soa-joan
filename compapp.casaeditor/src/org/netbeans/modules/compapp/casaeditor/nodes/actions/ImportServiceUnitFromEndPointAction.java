package org.netbeans.modules.compapp.casaeditor.nodes.actions;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.netbeans.modules.compapp.casaeditor.CasaDataEditorSupport;
import org.netbeans.modules.compapp.casaeditor.CasaDataNode;
import org.netbeans.modules.compapp.casaeditor.CasaDataObject;
import org.netbeans.modules.compapp.casaeditor.model.casa.CasaEndpointRef;
import org.netbeans.modules.compapp.casaeditor.model.casa.CasaServiceEngineServiceUnit;
import org.netbeans.modules.compapp.casaeditor.model.casa.CasaWrapperModel;
import org.netbeans.modules.compapp.casaeditor.nodes.CasaNode;
import org.netbeans.modules.xml.xam.dom.Attribute;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This is action used to create an external module as a reference to another service
 * using the endPoints.xml generated during the build
 * @author David
 */
public class ImportServiceUnitFromEndPointAction extends NodeAction {
    
    private static final String ENDPOINTS_XSD_URL =
            "nbres:/org/netbeans/modules/compapp/casaeditor/resources/endPoints.xsd";
    
    final public static String IMPORTED_ENDPOINTS_FILE = "importedEndPoints.xml";
    
    //the name of the xml nodes in the file to create
    final public static String IMPORTED_FILE_ROOT_NODE = "endPointsPool";
    final public static String CONSUMERS_NODE = "consumes";
    final public static String PROVIDERS_NODE = "provides";
    final public static String ENDPOINT_NODE = "endpoint";
    final public static String ENDPOINT_NAME_NODE = "endpoint-name";
    final public static String SERVICE_NAME_NODE = "service-name";
    final public static String INTERFACE_NAME_NODE = "interface-name";
    final public static String ENDPOINTS_DESCRIPTION_NODE = "description";
    //the name of the xml nodes in the source file
    final public static String PROJECT_NAME_SOURCE_NODE = "project-name";
    final public static String TEAM_SOURCE_NODE = "team";
    final public static String TEAM_NAME_SOURCE_NODE = "name";
    final public static String PROVIDERS_SOURCE_NODE = "provides";
    final public static String CONSUMERS_SOURCE_NODE = "consumes";
    final public static String ENDPOINT_SOURCE_NODE = "endpoint";
    final public static String TYPE_SOURCE_NODE = "type";
    final public static String ENDPOINTS_NAME_SOURCE_NODE = "endpoint-name";
    final public static String SERVICE_NAME_SOURCE_NODE = "service-name";
    final public static String INTERFACE_NAME_SOURCE_NODE = "interface-name";
    final public static String NAMESPACE_SOURCE_NODE = "namespace";
    final public static String LOCALNAME_SOURCE_NODE = "localname";
    final public static String ENDPOINTS_DESCRIPTION_SOURCE_NODE = "description";
    final public static String CONSUMER_TYPE = "consumer";
    
    /**
     * The path of the root of the CA project where the file should be imported
     */
    String projectPath;

    @Override
    protected boolean enable(org.openide.nodes.Node[] activatedNodes) {
        return true;
    }
    
    @Override
    protected boolean asynchronous() {
        return false;
    }
    
    @Override
    public String getName() {
        return NbBundle.getMessage(ImportServiceUnitFromEndPointAction.class, "LBL_ImportServiceReference_Name"); // NOI18N
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public Action getAction() {
        return this;
    }
    /**
     * action : open browse window, copy the selected xml file
     * @param e 
     */
    @Override
    public void performAction(org.openide.nodes.Node[] activatedNodes){
        //check activated nodes
        if (activatedNodes.length < 1) {
            return;
        }
        
        //get the casa model
        CasaWrapperModel cmodel = null;
        if (activatedNodes[0] instanceof CasaDataNode) {
            final CasaDataNode node = ((CasaDataNode) activatedNodes[0]);
            CasaDataObject obj = (CasaDataObject) node.getDataObject();
            CasaDataEditorSupport es = obj.getLookup().lookup(CasaDataEditorSupport.class);
            if (es != null) {
                cmodel = es.getModel();
            }
        } else if (activatedNodes[0] instanceof CasaNode) {
            final CasaNode node = ((CasaNode) activatedNodes[0]);
            cmodel = node.getModel();
        }

        if (cmodel == null) {
            notifyError("Impossible to find the CASA model, the endPoints cannot be created");
            return;
        }

        final CasaWrapperModel model = cmodel;
        
        //get the project path from the model
        this.projectPath=model.getJBIProject().getProjectDirectory().getPath();
        
        //set up the browse windows
        JFileChooser browser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("xml Files","xml");
        browser.setFileFilter(filter);
        //show it
        int returnVal = browser.showOpenDialog(null);
        
        
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            //a file has been selected
            File file=browser.getSelectedFile();
            
            //XML Validation
            try{
                URL url = new URI(ENDPOINTS_XSD_URL).toURL();
                SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                Schema schema = factory.newSchema(new StreamSource(url.openStream()));
                Validator validator = schema.newValidator();
                validator.validate(new StreamSource(file));
            }catch(SAXException ex){
                //the XML is not valid
                notifyError("The selected xml file is not valid, have you selected the right file"
                        + " (XXX-endpoints.xml) ? If you have, contact the file provider.\nException message : "+
                        ex.getMessage());                
                return;
            }catch(URISyntaxException ex){//TODO warning                
                ex.printStackTrace();
            }catch(MalformedURLException ex){
                ex.printStackTrace();
            }catch(IOException ex){
                ex.printStackTrace();
            }catch(Exception ex){
                ex.printStackTrace();
            }
            
            try{
                //creation of the source and dest doc
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document sourceDoc =  dBuilder.parse(file);
                Document destDoc=null;
                //in order to get the destDoc, we check if it has been created before
                String destPath=projectPath+File.separator+"src"+File.separator+"conf"+File.separator+IMPORTED_ENDPOINTS_FILE;
                File destfile = new File(destPath);
                
                if(destfile.exists() && !destfile.isDirectory()) { 
                    //the file has already been created so we just get the Document
                    try{
                        destDoc = dBuilder.parse(destfile);
                    }catch(SAXException ex){
                        ex.printStackTrace();
                    }
                }else{
                    //the xml file does not exist yet so we create it and write the structure                 
                    destDoc = dBuilder.newDocument();
                    //create the structure of the doc
                    Element root = destDoc.createElement(IMPORTED_FILE_ROOT_NODE);
                    destDoc.appendChild(root);                    
                }
                //So far we have our sourceDoc and our destDoc ready for treatment
                
                //TODO add the source project name to the endpoint
                userEndPointsSelection(sourceDoc, model);
                
                //copy the provider endpoints from source to dest
//                String projectSource = sourceDoc.getElementsByTagName(PROJECT_NAME_SOURCE_NODE).item(0).getTextContent();
//                Element Team = (Element) sourceDoc.getElementsByTagName(TEAM_SOURCE_NODE).item(0);
//                String teamName = Team.getElementsByTagName(TEAM_NAME_SOURCE_NODE).item(0).getTextContent();
//                NodeList endpoints = sourceDoc.getElementsByTagName(ENDPOINT_SOURCE_NODE);
//                Element destRoot = (Element) destDoc.getElementsByTagName(IMPORTED_FILE_ROOT_NODE).item(0);
//                for (int i=0 ; i<endpoints.getLength() ; i++){
//                    //for each endPoint, we clone it, change its document, check if it already exists in the dest, write it to the dest
//                    Node copiedEP=endpoints.item(i).cloneNode(true);
//                    destDoc.adoptNode(copiedEP);
//                    //we add the required field in the endpoint node
//                    Node projectName = destDoc.createElement("project-name");
//                    projectName.setTextContent(projectSource);
//                    copiedEP.appendChild(projectName);  
//                    Node teamNameNode = destDoc.createElement("team-name");
//                    teamNameNode.setTextContent(teamName);
//                    copiedEP.appendChild(teamNameNode);
//                    //check if node already exists
//                    boolean exist = false;
//                    NodeList existingEndPoints = destRoot.getElementsByTagName(ENDPOINT_NODE);
//                    for (int j=0 ; j<existingEndPoints.getLength() ; j++){
//                        if(existingEndPoints.item(j).isEqualNode(copiedEP)){
//                            exist=true;
//                        }
//                    }
//                    //Node projectName = destDoc.createElement("from-project");
//                    //copiedEP.appendChild(projectName);
//                    if(!exist){
//                        destRoot.appendChild(copiedEP);
//                    }
//                }               
//            
//                //write destDoc to file
//                //FIXME incorrect indentation
//                TransformerFactory transformerFactory = TransformerFactory.newInstance();
//		Transformer transformer = transformerFactory.newTransformer();
//                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
//                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
//                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
//		DOMSource source = new DOMSource(destDoc);
//		StreamResult result = new StreamResult(destfile);
//                transformer.transform(source, result);
                
                
            }catch(IOException ioexpection){
                ioexpection.printStackTrace();
            }catch(SAXException ex){
                ex.printStackTrace();
            }catch(ParserConfigurationException ex){
                ex.printStackTrace();
            }catch(NullPointerException ex){
                ex.printStackTrace();
            }/*catch(TransformerException ex){
                ex.printStackTrace();
            }*/
            
        }
    }
    /**
     * @author DavidD
     * @param endPoints 
     * @return 
     */
    private void userEndPointsSelection(Document sourceDoc, final CasaWrapperModel model){
        List<ImportedEndPoint> EPtoSelection = new ArrayList<ImportedEndPoint>();
        //get project and team name
        Element project = (Element) sourceDoc.getElementsByTagName("project").item(0);
        String projectName = project.getElementsByTagName("name").item(0).getTextContent();
        String projectVersion = project.getElementsByTagName("version").item(0).getTextContent();
        String projectDescription = project.getElementsByTagName("description").item(0).getTextContent();
        Element teamElement = (Element) sourceDoc.getElementsByTagName(TEAM_SOURCE_NODE).item(0);
        String teamName = teamElement.getElementsByTagName(TEAM_NAME_SOURCE_NODE).item(0).getTextContent();
        String teamContact = teamElement.getElementsByTagName("contact").item(0).getTextContent();
        //get all the endpoints nodes from the DOM
        NodeList endpointsNodes = sourceDoc.getElementsByTagName(ENDPOINT_SOURCE_NODE);   
        if(endpointsNodes.getLength()==0){
            notifyError("There is no endPoints in the selected file.");
            return;
        }
        for (int i=0 ; i<endpointsNodes.getLength() ; i++){
            //conscruct to endpoint by extracting the info from the endPoint element
            Element endPointElement = (Element) endpointsNodes.item(i);
            
            boolean isConsumer = endPointElement.getElementsByTagName(TYPE_SOURCE_NODE).item(0).getTextContent().equals(CONSUMER_TYPE);
            
            String endPointName = endPointElement.getElementsByTagName(ENDPOINTS_NAME_SOURCE_NODE).item(0).getTextContent();
            
            Element endPointServiceNSelem = (Element) endPointElement.getElementsByTagName(SERVICE_NAME_SOURCE_NODE).item(0);
            String serviceNS = endPointServiceNSelem.getElementsByTagName(NAMESPACE_SOURCE_NODE).item(0).getTextContent();
            String serviceLN = endPointServiceNSelem.getElementsByTagName(LOCALNAME_SOURCE_NODE).item(0).getTextContent();
            QName serviceQname = new QName(serviceNS, serviceLN);
            
            Element endPointInterfaceNSelem = (Element) endPointElement.getElementsByTagName(INTERFACE_NAME_SOURCE_NODE).item(0);
            String interfaceNS = endPointInterfaceNSelem.getElementsByTagName(NAMESPACE_SOURCE_NODE).item(0).getTextContent();
            String interfaceLN = endPointInterfaceNSelem.getElementsByTagName(LOCALNAME_SOURCE_NODE).item(0).getTextContent();
            QName interfaceQname = new QName(interfaceNS, interfaceLN);
                        
            ImportedEndPoint endPoint = new ImportedEndPoint(isConsumer, endPointName, interfaceQname, serviceQname, projectDescription, projectName,projectVersion, teamName, teamContact);
            EPtoSelection.add(endPoint);
        }
        final MultipleEndPointsSelectionPanel panel = new MultipleEndPointsSelectionPanel(EPtoSelection);
        
        DialogDescriptor descriptor = new DialogDescriptor(
                panel,
                NbBundle.getMessage(ImportServiceUnitFromEndPointAction.class, "LBL_Multiple_Endpoints_Selection_Panel"),   // NOI18N
                true,
                new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getSource().equals(DialogDescriptor.OK_OPTION)) {
                    createSelectedEndPoints(panel.getSelectedEndPoints(), model);
                }
            }
        });
        Dialog dlg = DialogDisplayer.getDefault().createDialog(descriptor);
        dlg.setPreferredSize(new Dimension(400, 400));
        dlg.setVisible(true);
    }
    
    private void createSelectedEndPoints(final List<ImportedEndPoint> endPoints, final CasaWrapperModel model){
        if(endPoints==null){
            throw new IllegalArgumentException("endPoints is null");
        }
        if(endPoints.size()==0){
            notifyError("You have not selected any endPoint !");
            return;
        }
        
        final List<CasaEndpointRef> existingEP=model.getAllEndpointsRef();
        //need to run in another thread because of the synchronous call to swingUtilities
        Thread appThread = new Thread() {
            public void run() {
                try {
                    for(ImportedEndPoint ep : endPoints){
                        createSelectedEndPoint(ep, existingEP, model);
                    }  
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        };
        appThread.start();             
    }
    
    private boolean createSelectedEndPoint(final ImportedEndPoint ep, List<CasaEndpointRef> existingEP,final CasaWrapperModel model){
        boolean exists=false;
        for (CasaEndpointRef ref : existingEP){
            if(testEquality(ep, ref)){
                exists=true;
            }
        }
        if(exists){
            notifyError("The endPoint \""+ep.getEndPointName()+"\" already exists in this application, impossible to add the same endPoint.");
            return false;
        }
        final String projectName = ep.getProjectName();
        //test if a SU with teamName as display-name exists
        boolean projectExists=false;
        List<CasaServiceEngineServiceUnit> SUs = model.getServiceEngineServiceUnits();
        for(final CasaServiceEngineServiceUnit su : SUs){
           String name = su.getUnitName();
           if(name.equals(projectName)){
                projectExists=true;
                SwingUtilities.invokeLater(new Runnable(){
                    @Override
                    public void run(){
                        try{                           
                            CasaEndpointRef endPoint = model.addExternalEndpoint(su, ep.isConsumer());
                            if(!model.startTransaction()){
                                throw new Exception("The transaction could not be started");
                            }else{
                                endPoint.getEndpoint().get().setEndpointName(ep.getEndPointName());
                                endPoint.getEndpoint().get().setInterfaceQName(ep.getInterfaceName());
                                endPoint.getEndpoint().get().setServiceQName(ep.getServiceName());                            
                                model.endTransaction();
                                model.fireReload();
                            }                             
                        }catch(Exception ex){
                            ex.printStackTrace();
                            //TODO action on fail
                        }
                    }
                });
            }
        }
        if(!projectExists){
            //need to invoke synchronously. otherwise the next endPoint will not find the new SU 
            //because it will not be created yet and will created another SU with the same team name
            try{
                SwingUtilities.invokeAndWait(new Runnable(){
                    @Override
                    public void run(){
                        try{ 
                            CasaServiceEngineServiceUnit su = model.addServiceEngineServiceUnitFromPalette(false, 40, 40, null,null,null,null);

                            CasaEndpointRef endPoint = model.addExternalEndpoint(su, ep.isConsumer());
                            if(!model.startTransaction()){
                                throw new Exception("The transaction could not be started");
                            }else{
                                su.setUnitName(projectName);
                                su.setDescription(ep.getProjectDescription());
                                su.setTeamName(ep.getTeamName());
                                su.setContact(ep.getTeamContact());
                                su.setVersion(ep.getProjectVersion());
                                endPoint.getEndpoint().get().setEndpointName(ep.getEndPointName());
                                endPoint.getEndpoint().get().setInterfaceQName(ep.getInterfaceName());
                                endPoint.getEndpoint().get().setServiceQName(ep.getServiceName());                            
                                model.endTransaction();
                                model.fireReload();
                            }                             
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                });
            }catch(InterruptedException e){
                notifyError("the operation of creating a new service unit has been interrupted, please try again");
                e.printStackTrace();
            }catch(InvocationTargetException e){
                e.printStackTrace();
            }
        }
        
        return true;
    }
    private void notifyError(String message){
        NotifyDescriptor d = new NotifyDescriptor.Message(
                            message,
                            NotifyDescriptor.ERROR_MESSAGE);
        DialogDisplayer.getDefault().notify(d);
    }
    
    private boolean testEquality(ImportedEndPoint myEP, CasaEndpointRef ref){
        return(myEP.getEndPointName().equals(ref.getEndpointName())
               && myEP.getInterfaceName().equals(ref.getInterfaceQName())
               && myEP.getServiceName().equals(ref.getServiceQName()));
    }
    
}