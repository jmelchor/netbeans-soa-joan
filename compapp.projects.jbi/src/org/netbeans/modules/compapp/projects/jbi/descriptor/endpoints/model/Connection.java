/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.compapp.projects.jbi.descriptor.endpoints.model;

import java.io.Serializable;

/**
 * JBI service connection
 *
 * @author tli
 */
public class Connection implements Serializable {
    //ESBNBM-99: Set route type and routing rule per default
    public static final String ROUTE_TYPE_DEFAULT = "direct";
    public static final String ROUTING_RULE_DEFAULT = "default";
    public static final String MODE_DEFAULT = "local" ; 
    /**
     * DOCUMENT ME!
     */
    private Endpoint consume;

    /**
     * DOCUMENT ME!
     */
    private Endpoint provide;

    private String routeType;
    private String routingRule;
    private String mode;
    /**
     * DOCUMENT ME!
     *
     * @param consume
     * @param provide
     */
    public Connection(Endpoint consume, Endpoint provide) {
        this.consume = consume;
        this.provide = provide;
        //ESBNBM-99: Set route type and routing rule per default
        this.routeType = ROUTE_TYPE_DEFAULT;
        this.routingRule = ROUTING_RULE_DEFAULT ;
        this.mode = MODE_DEFAULT;
    }
    
    public Connection(Endpoint consume, Endpoint provide, String routeType, String routingRule, String mode) {
        this.consume = consume;
        this.provide = provide;
        this.routeType=routeType;        
        this.routingRule=routingRule;
        this.mode=mode;
        //ESBNBM-99: Set route type and routing rule per default
        if (this.routeType == null | this.routeType.length() == 0) {
            this.routeType=ROUTE_TYPE_DEFAULT;
        }
        if (this.routingRule == null | this.routingRule.length() == 0) {
            this.routingRule=ROUTING_RULE_DEFAULT;
        }
        
        if (this.mode == null | this.mode.length() == 0) {
            this.mode=MODE_DEFAULT;
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @return Returns the consume endpoint.
     */
    public Endpoint getConsume() {
        return consume;
    }

    /**
     * DOCUMENT ME!
     *
     * @return Returns the provide endpoint.
     */
    public Endpoint getProvide() {
        return provide;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }

    public String getRoutingRule() {
        return routingRule;
    }

    public void setRoutingRule(String routingRule) {
        this.routingRule = routingRule;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }    

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Connection other = (Connection) obj;
        
        if (this.consume != other.consume && (this.consume == null || !this.consume.equals(other.consume))) {
            return false;
        }
        if (this.provide != other.provide && (this.provide == null || !this.provide.equals(other.provide))) {
            return false;
        }
 
        //ESBNBM-99: Set route type and routing rule per default
        // Change the equal method
        // Start the comparison with routeType
        boolean compareRouteType = this.compareProperties(this.routeType, other.routeType, ROUTE_TYPE_DEFAULT);
        if (!compareRouteType) {
            return false ; 
        }
                        
        boolean compareRoutingRule = this.compareProperties(this.routingRule, other.routingRule, ROUTING_RULE_DEFAULT);
        if (!compareRoutingRule) {
            return false ; 
        }
        
        boolean compareMode = this.compareProperties(this.mode, other.mode, MODE_DEFAULT);
        if (!compareMode) {
            return false ; 
        }
        
        return true;
    }
    
    public String toString() {
        String connectionProperties = ". Conenction properties: Route type= " + this.routeType + "; Routing rule= " + this.routeType + "; mode= " + this.mode ;
        return consume.getFullyQualifiedName() + " -> " + provide.getFullyQualifiedName() + connectionProperties; 
    }
    
    /**
     * ESBNBM-99: Set route type and routing rule per default
     * This method is used to compare the properties of the connection. 
     * Before the version 3.1.x Routing rule, rule type and mode did not exist
     * The comparison made by this method us take into account the empty or null 
     * element. 
     * So the compare follows the table 
     * 
     *         |null| empty | default | other than default |
     * -----------------------------------------------------
     * null    | OK | OK    | OK      | KO                 |
     * -----------------------------------------------------
     * empty   | OK | OK    | OK      | KO                 |
     * -----------------------------------------------------
     * default | OK | OK    | OK      | KO                 |
     * -----------------------------------------------------
     * Other   | KO | KO    | KO      | KO or OK if same   |
     * -----------------------------------------------------
     * 
     * @param thisProperty
     * @param otherProperty
     * @param byDefault
     * @return boolean 
     */
    
    private boolean compareProperties(String thisProperty, String otherProperty, String byDefault) {       
        // Change the equal method        
        boolean isThisEmpty;
        boolean isOtherEmpty;
        // Start the comparison with routeType
        isThisEmpty = thisProperty == null || thisProperty.trim().length() == 0 || thisProperty.trim().equalsIgnoreCase(byDefault.trim());
        isOtherEmpty = otherProperty == null || otherProperty.trim().length() == 0 || otherProperty.trim().equalsIgnoreCase(byDefault.trim());
        if (!(isThisEmpty & isOtherEmpty)) {
            if ((thisProperty== null  && otherProperty != null) || (thisProperty != null && otherProperty == null)) {
                return false;
            }            
            if (!thisProperty.trim().equalsIgnoreCase(otherProperty.trim())) {
                return false;
            }
        }
        return true;
    }
    
}
