/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package org.netbeans.modules.bpel.model.impl;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 20/04/18
 */
public class EnumMethodWrapper {
    private final Map<Enum<?>, Method> methods = new HashMap<>();

    public EnumMethodWrapper(
            final Class<? extends Enum> clazz, final String methodName, final Class<?>... parameterTypes) {
        super();

        init(clazz, methodName, parameterTypes);
    }

    public EnumMethodWrapper(
            final Class<?>[] classes,
            final String classname,
            final String methodName,
            final Class<?>... parameterTypes) {
        super();

        Class<? extends Enum> clazz = null;

        for (final Class c: classes) {
            if (c.getName().equals(classname) && Enum.class.isAssignableFrom(c)) {
                clazz = c;
            }
        }

        init(clazz, methodName, parameterTypes);
    }

    public Object call(final Enum<?> _enum, final Object... params) {
        try {
            return methods.get(_enum).invoke(_enum, params);
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    private void init(final Class<? extends Enum> clazz, final String methodName, final Class<?>... parameterTypes) {
        try {
            for (final Enum<?> _enum: clazz.getEnumConstants()) {
                final Method m = _enum.getClass().getMethod(methodName, parameterTypes);
                m.setAccessible(true);
                methods.put(_enum, m);
            }
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }
}
