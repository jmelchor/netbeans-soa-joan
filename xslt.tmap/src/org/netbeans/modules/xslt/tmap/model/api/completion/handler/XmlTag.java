/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.netbeans.modules.xslt.tmap.model.api.completion.handler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lativ
 */
public class XmlTag extends XmlElement {
    private List<XmlAttribute> mAttrs = new ArrayList<XmlAttribute>();
    private String mName;
    private XmlTag mParentTag;
    private List<XmlTag> mChildTags = new ArrayList<XmlTag>();
    private boolean isParentTagInit = false;
    private boolean isChildTagInit = false;

    public XmlTag(String tagName) {
        this(tagName, false);
    }
    
    public XmlTag(String tagName, boolean isComplete) {
        super();
        mName = tagName;
        setState(isComplete);
    }

    public String getTagName() {
        return mName;
    }
    
    public void setParent(XmlTag pTag) {
        if (isParentTagInit) {
            throw new IllegalStateException("parentTag yet init");
        }
        mParentTag = pTag;
        isParentTagInit = true;
    }
    
    public XmlTag getParent() {
//TODO add root tag type support        
//        if (!isParentTagInit) {
//            throw new IllegalStateException("parentTag not yet init");
//        }
        return mParentTag;
    }

    public void addAttribute(XmlAttribute attr) {
        if (attr == null) {
            return;
        }
        mAttrs.add(attr);
    }
    
    public List<XmlAttribute> getAttributes() {
        return mAttrs;
    }
    
    public XmlAttribute getAttribute(String name) {
        if (name == null || name.length() <1) {
            return null;
        }
        XmlAttribute searchedAttr = null;
        for (XmlAttribute attr : mAttrs) {
            if (name.equals(attr.getName())) {
                searchedAttr = attr;
                break;
            }
        }
        return searchedAttr;
    }
    
    public void addChildTag(XmlTag cTag) {
        if (cTag == null) {
            return;
        }
        mChildTags.add(cTag);
        isChildTagInit = true;
    }

    public List<XmlTag> getChildTags() {
//        if (!isChildTagInit) {
//            throw new IllegalStateException("childTags not yet init");
//        }
        return mChildTags;
    }

    public void setChildTags(List<XmlTag>  childTags) {
        if (isChildTagInit) {
            throw new IllegalStateException("childTags yet init");
        }
        mChildTags = childTags;
        isChildTagInit = true;
    }
    
}
