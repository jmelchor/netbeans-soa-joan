pipeline {
    agent {
        label "master"
    }
    tools {
        ant "Ant"
        jdk "JDK8"
    }
    environment {
        BRANCH="release/${version}"
        VERSION="${version}.${BUILD_NUMBER}"
    }
    stages {
        stage("Initialize") {
            steps {
                wrap([$class: 'BuildUser']) {
                    buildName "${VERSION}"
                    buildDescription "Executed on branch ${branch} @ Jenkins node ${NODE_NAME} by ${BUILD_USER}"
                }
                sh 'echo "Running Script v0.3"'
            }
        }
        stage("Clone code from VCS") {
            steps {
                script {
                    git 'git@bitbucket.org:openesb/netbeans-soa.git'
                    sh "git checkout ${BRANCH}"
                    sh "git pull"
                }
            }
        }
        stage("Ant Build") {
            steps {
                sh "sed -ie 's/^app.version=.*/app.version=$VERSION/' nbproject/project.properties"
                sh "sed -ie 's/^currentVersion=.*/currentVersion=OpenESB Studio Community Edition $VERSION/' branding/core/core.jar/org/netbeans/core/startup/Bundle.properties"
                sh "sed -ie 's/^CTL_MainWindow_Title=.*/CTL_MainWindow_Title=OpenESB Studio Community Edition $VERSION/' branding/modules/org-netbeans-core-windows.jar/org/netbeans/core/windows/view/ui/Bundle.properties"
                sh "sed -ie 's/^CTL_MainWindow_Title_No_Project=.*/CTL_MainWindow_Title_No_Project=OpenESB Studio Community Edition $VERSION/' branding/modules/org-netbeans-core-windows.jar/org/netbeans/core/windows/view/ui/Bundle.properties"

                sh 'ant'
                sh 'ant build-zip'
            }
        }
        stage("Publish to Nexus Repository Manager") {
            steps {
                script {
                    filesByGlob = findFiles(glob: "dist/soa.zip")
                    echo "*** File: ${filesByGlob}";

                    nexusArtifactUploader(
                        nexusVersion: "nexus3",
                        protocol: "https",
                        nexusUrl: "nexus.open-esb.net",
                        groupId: "net.open-esb.netbeans-soa",
                        version: "$VERSION",
                        repository: "maven-releases",
                        credentialsId: "nexus-credentials",
                        artifacts: [
                            [artifactId: 'dist',
                            classifier: '',
                            file: filesByGlob[0].path,
                            type: 'zip']
                        ]
                    );
                }
            }
        }
        stage("Tag Git") {
            steps {
                sh "git tag $VERSION"
                sh "git push origin --tags"
            }
        }
    }
    post {
         always {
             echo 'This will always run'
         }
         success {
             echo 'This will run only if successful'
         }
         failure {
             mail bcc: '', body: "<b>Example</b><br>Project: ${env.JOB_NAME} <br>Build: ${VERSION} <br> URL de build: ${env.BUILD_URL}", cc: '', charset: 'UTF-8', from: 'noreply@pymma.com', mimeType: 'text/html', replyTo: '', subject: "ERROR CI: Project name -> ${env.JOB_NAME}", to: "paul.perez@pymma.com,yoram.halberstam@gmail.com";
         }
         unstable {
             echo 'This will run only if the run was marked as unstable'
         }
         changed {
             echo 'This will run only if the state of the Pipeline has changed'
             echo 'For example, if the Pipeline was previously failing but is now successful'
         }
    }
}